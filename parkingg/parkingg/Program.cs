﻿using System;
using System.Threading;

namespace parkingg
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            Car Mersedes = new Car(rnd.Next(5, 10), "Mersedes");
            Car Kia = new Car(rnd.Next(5, 10), "Mersedes");
            Truck truck = new Truck(rnd.Next(5, 10), "Mersedes");
            Bike Yamaha = new Bike(rnd.Next(5, 10), "Mersedes");
            Bus MersedesBenz = new Bus(rnd.Next(5, 10), "Mersedes");
            Parking parking = Parking.Constructor();
            parking.AddCar(Mersedes);
            parking.AddCar(Kia);
            parking.AddCar(truck);
            parking.AddCar(Yamaha);
            parking.AddCar(MersedesBenz);
            Thread parkingWork = new Thread(parking.Working);

            ConsoleCommands commands = new ConsoleCommands(parking);
            parkingWork.Start();

            commands.StartExecute(parking);
            Console.ReadKey();
        }
        static void Polozhit(object obj, Transaction trn)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(trn);
            Console.ForegroundColor = ConsoleColor.White;
        }
        static void Snyat(object obj, Transaction trn)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(trn);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
