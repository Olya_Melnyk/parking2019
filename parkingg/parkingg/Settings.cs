﻿using System;
using System.Collections.Generic;
using System.Text;

namespace parkingg
{
    static class Settings
    {
        static public double StartBalance { get; private set; } = 0;
        static public int ParkingSize { get; private set; } = 10;
        static public int PayTime { get; private set; } = 5;
        static public double CarPay { get; private set; } = 2;
        static public double BusPay { get; private set; } = 3.5;
        static public double TruckPay { get; private set; } = 5;
        static public double BikePay { get; private set; } = 1;
        static public double Fine { get; private set; } = 2.5;
    }
}
