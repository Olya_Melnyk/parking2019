﻿using System;
using System.Collections.Generic;
using System.Text;

namespace parkingg
{
    class Bike : Transport
    {
        public Bike() : this(0)
        {

        }
        public Bike(double Balance) : this(Balance, "")
        {

        }
        public Bike(double Balance, string Model)
        {
            PriceOfParking = Settings.BikePay;
            this.Balance = Balance;
            this.Model = Model;
        }
    }
}
