﻿using System;
using System.Collections.Generic;
using System.Text;

namespace parkingg
{
    class Car : Transport
    {
        public Car() : this(0)
        {

        }
        public Car(double Balance) : this(Balance, "")
        {

        }
        public Car(double Balance, string Model)
        {
            PriceOfParking = Settings.CarPay;
            this.Balance = Balance;
            this.Model = Model;
        }
    }
}
