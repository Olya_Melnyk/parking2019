﻿using System;
using System.Collections.Generic;
using System.Text;

namespace parkingg
{
    class Truck : Transport
    {
        public Truck() : this(0)
        {

        }
        public Truck(double Balance) : this(Balance, "")
        {

        }
        public Truck(double Balance, string Model)
        {
            PriceOfParking = Settings.TruckPay;
            this.Balance = Balance;
            this.Model = Model;
        }
    }
}
