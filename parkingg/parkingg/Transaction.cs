﻿using System;
using System.Collections.Generic;
using System.Text;

namespace parkingg
{
    class Transaction
    {
        public Transaction(double Size, Type KindOfTransport)
        {
            Time = DateTime.Now;
            this.Size = Size;
            this.KindOfTransport = KindOfTransport.Name;
            Logfile.WriteTransaction(this);
        }
        public DateTime Time { get; private set; }
        public string KindOfTransport { get; private set; }
        public double Size { get; private set; }

        public override string ToString()
        {
            string transactionInfo = "";
            transactionInfo += "Time:\t" + Time;
            transactionInfo += "\tSize:\t" + Size;
            transactionInfo += "\tTransport:\t" + KindOfTransport;
            transactionInfo += "\tID\t" + Math.Abs(GetHashCode());
            return transactionInfo;
        }
        public override int GetHashCode()
        {
            return (Time.ToString() + Size.ToString() + KindOfTransport.ToString()).GetHashCode();
        }
    }
}
