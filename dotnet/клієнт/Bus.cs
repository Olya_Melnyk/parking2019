﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  ASP.NET_Core
{
    class Bus : Transport
    {
        public Bus() : this(0)
        {

        }
        public Bus(double Balance) : this(Balance, "")
        {

        }
        public Bus(double Balance, string Model)
        {
            PriceOfParking = Settings.BusPay;
            this.Balance = Balance;
            this.Model = Model;
        }
    }
}
