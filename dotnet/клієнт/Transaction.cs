﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;


namespace ASP.NET_Core{//.Services{
    public class Transaction
    {
        public Transaction(double Size, Type KindOfTransport)
        {
            Time = DateTime.Now;
            this.Size = Size;
            this.KindOfTransport = KindOfTransport.Name;
            Logfile.WriteTransaction(this);
        }
        [JsonProperty("time")]
        public DateTime Time { get; private set; }
        [JsonProperty("Kind of transport")]
        public string KindOfTransport { get; private set; }
        [JsonProperty("size")]
        public double Size { get; private set; }
        [JsonProperty("id")]
        public string id{get;private set;}
        
        public override string ToString()
        {
            string transactionInfo = "";
            transactionInfo += "Time:\t" + Time;
            transactionInfo += "\tSize:\t" + Size;
            transactionInfo += "\tTransport:\t" + KindOfTransport;
            transactionInfo += "\tID\t" + Math.Abs(GetHashCode());
            return transactionInfo;
        }
        public override int GetHashCode()
        {
            return (Time.ToString() + Size.ToString() + KindOfTransport.ToString()).GetHashCode();
        }
    }
}
