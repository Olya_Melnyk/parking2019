﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ASP.NET_Core
{
    static class Logfile
    {
        static string filename = @"C:\Users\Admin\source\repos\parkingg\logfile.txt";
        public static void WriteTransaction(Transaction transaction)
        {
            using (StreamWriter streamWriter = new StreamWriter(filename, true))
            {
                streamWriter.WriteLine(transaction.ToString());
            }

        }
        public static string ReadAllTransactions()
        {
            string fileContent = "";
            using (StreamReader streamReader = new StreamReader(filename))
            {
                fileContent = streamReader.ReadToEnd();
            }
            return fileContent;
        }
    }
}
