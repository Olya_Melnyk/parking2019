﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASP.NET_Core
{
    class Parking : SingleParking 
    {
        List<Transport> cars = new List<Transport>();
        Parking()
        {

        }
        public static Parking Constructor()
        {
            if (instance == null)
            {
                instance = new Parking();
            }
            return (Parking)instance;
        }

        public bool AddCar(Transport transport)
        {
            transport.Withdrawn += TopUp;
            transport.ToppedUp += delegate (object sender, Transaction transaction)
            {
                transport.Balance -= transport.Debt;
                if (transport.Balance < 0)
                {
                    transport.Debt = -1 * transport.Balance;
                    transport.Balance = 0;
                }
                else
                {
                    transport.Debt = 0;
                }

            };
            if (cars.Count < Settings.ParkingSize)
            {
                cars.Add(transport);
                return true;
            }
            return false;
        }
        public bool RemoveCar(Transport transport)
        {
            return cars.Remove(transport);
        }
        public int CountFree()
        {
            return Settings.ParkingSize - cars.Count;
        }
        async public void Working()
        {
            for (int i = 0; i < cars.Count; i++)
            {
                cars[i].Withdraw(this, new Transaction(cars[i].PriceOfParking, cars[i].GetType()));
            }
            System.Threading.Thread.Sleep(1000 * Settings.PayTime);
            await System.Threading.Tasks.Task.Run(() => Working());
        }

        public override string ToString()
        {
            string parking = $"Balance: {Balance}\n";
            parking += $"Number of cars {cars.Count}\n";
            parking += $"Max number of cars {Settings.ParkingSize}\n";
            for (int i = 0; i < cars.Count; i++)
                parking += $"{i + 1}\t{cars[i].GetType().Name}\t{cars[i].Model}\t{cars[i].Balance}\t\t{cars[i].Debt}\n";
            return parking;
        }
        
    }
}
