﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  ASP.NET_Core
{
    class Bike : Transport
    {
        public Bike() : this(0)
        {

        }
        public Bike(double Balance) : this(Balance, "")
        {

        }
        public Bike(double Balance, string Model)
        {
            PriceOfParking = Settings.BikePay;
            this.Balance = Balance;
            this.Model = Model;
        }
    }
}
