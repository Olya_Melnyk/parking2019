﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  ASP.NET_Core
{
abstract class Transport : BankAccount
{
    public double PriceOfParking { get; protected set; }
    public double Debt { get; set; }//борг

    public string Model { get; protected set; }
    public override void Withdraw(object sender, Transaction transaction)
    {
        if (Balance < transaction.Size)
        {
            if (Balance != 0)
            {
                //Если мы хотим снять больше денег, чем на балансе
                //снимаем все что есть и остальное записываем в долг
                Debt += (transaction.Size - Balance) * Settings.Fine; //добавляем долг машине и умножаем на коэффициент штрафа
                base.Withdraw(sender, new Transaction(Balance, GetType()));
                Balance = 0;
            }
            else
            {
                //Если мы хотим снять больше денег, чем на балансе
                //и на балансе нет денег совсем, просто добавляем снимаемую сумму в долг
                Debt += transaction.Size * Settings.Fine;
            }
        }
        else
        {
            //Если все ОК то просто снимаем деньги
            base.Withdraw(sender, transaction);
        }

    }

}
}
