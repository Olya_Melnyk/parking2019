﻿using System;
using System.Collections.Generic;
using System.Text;
//using ASP.NET_Core_Lecture.Services;

namespace  ASP.NET_Core
{
    delegate void AccountHandler(object sender, Transaction transaction);
    abstract class BankAccount
    {
        public event AccountHandler ToppedUp;
        public event AccountHandler Withdrawn;

        public double Balance { get; set; }

        public virtual void TopUp(object sender, Transaction transaction)
        {
            Balance += transaction.Size;
            ToppedUp?.Invoke(this, transaction);
        }

        public virtual void Withdraw(object sender, Transaction transaction)
        {
            Balance -= transaction.Size;
            Withdrawn?.Invoke(this, transaction);
        }
    }
}
