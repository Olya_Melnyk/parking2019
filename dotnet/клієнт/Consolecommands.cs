﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASP.NET_Core
{
    class ConsoleCommands
    {
        Parking current_parking;
        public ConsoleCommands(Parking current_parking)
        {
            this.current_parking = current_parking;
        }
        public void StartExecute(Parking parking)
        {
            Console.WriteLine("Hello, welcom to parking!");
            Console.WriteLine("input number operation");
            Console.WriteLine("get balance of parking(1)|sum parking of last minute(2)|number free places(3)");
            Console.WriteLine( "show transaction for the last minute(4)|all transports on parking(6)" );
            Console.WriteLine("Add transport(7)|Exit(0)");
            string command = Console.ReadLine();
            switch (command)
            {
                case "1":
                    Console.WriteLine(parking.Balance);
                    break;
                case "2":
                    Console.WriteLine(Settings.StartBalance);
                    break;
                case "3":
                    Console.WriteLine(parking.CountFree());
                    break;
                case "4":
                    Console.WriteLine(current_parking);
                    StartExecute(parking);
                    break;
                case "6":
                    Console.WriteLine(parking);
                    break;
                case "7":
                    Console.WriteLine("input kind of your transport:car||bike||truck||bus ");
                    string s = Console.ReadLine();
                    switch(s)
                    {
                        case "car":
                            parking.AddCar(new Car());
                            break;
                        case "bike":
                            parking.AddCar(new Bike());
                            break;
                        case "truck":
                            parking.AddCar(new Truck());
                            break;
                        case "bus":
                            parking.AddCar(new Bus());
                            break;
                    }
                    break;
                case "0":
                    Console.WriteLine("exit");
                    return;
            }
        }
    }
}
