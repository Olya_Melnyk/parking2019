using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System;

namespace ASP.NET_Core {
    public class TransactionsService{
    private HttpClient _client;

    public TransactionsService() {
      _client = new HttpClient();
    }

    public async Task<List<Transaction>> GetTransactions() {
      var transactions = await _client.GetStringAsync($"logfile.txt");
      return JsonConvert.DeserializeObject<List<Transaction>> (transactions);
    }

    public async Task<Transaction> GetTransaction(DateTime time) {
      var transaction = await _client.GetStringAsync($"logfile.txt{time}");
      return JsonConvert.DeserializeObject<Transaction> (transaction);
    }
  }

  
}