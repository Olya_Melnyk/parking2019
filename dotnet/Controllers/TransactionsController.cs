using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using ASP.NET_Core_Lecture.Services;
//using ASP.NET_Core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ASP.NET_Core{
  public class TransactionsController: ControllerBase {
    private TransactionsService transactionsService;
    
    public TransactionsController(){//ITransactionsService service) {
      transactionsService =new TransactionsService();
    }

    // GET api/values
    [HttpGet]
    public async Task<ActionResult<List<Transaction>>> Get() {
      return Ok(await transactionsService.GetTransactions());
    }

    // GET api/values/5
    [HttpGet("{time}")]
    public async Task<ActionResult<Transaction>> Get(DateTime time) {
      return Ok(await transactionsService.GetTransaction(time));
    }
  }
}