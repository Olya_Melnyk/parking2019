using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using ASP.NET_Core_Lecture.Services;
using Microsoft.AspNetCore.Mvc;
//using System.Collections.Generic;

namespace ASP.NET_Core.Interface{
    public interface ITransactionsService
    {
        Task <List<Transaction>> GetTransactions();
        Task <Transaction> GetTransaction(DateTime time);
    }
}
