﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Threading;


namespace ASP.NET_Core
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddTransient<TransactionsService>();
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.Use(async(context, next) => {
                //Console.WriteLine("Started handling request");
                Work();
                 await next.Invoke();
                    Console.WriteLine("Finished handling request");
              });
            app.UseMvc(routes  =>
            {
                routes.MapRoute("default","{controller=Home}/{action=Index}/{time?}");
            });
        }
             public void  Work()
             {Random rnd = new Random();
            Car Mersedes = new Car(rnd.Next(5, 10), "Mersedes");
            Car Kia = new Car(rnd.Next(5, 10), "Mersedes");
            Truck truck = new Truck(rnd.Next(5, 10), "Mersedes");
            Bike Yamaha = new Bike(rnd.Next(5, 10), "Mersedes");
            Bus MersedesBenz = new Bus(rnd.Next(5, 10), "Mersedes");
            Parking parking = Parking.Constructor();
            parking.AddCar(Mersedes);
            parking.AddCar(Kia);
            parking.AddCar(truck);
            parking.AddCar(Yamaha);
            parking.AddCar(MersedesBenz);
            Thread parkingWork = new Thread(parking.Working);

            ConsoleCommands commands = new ConsoleCommands(parking);
            parkingWork.Start();

            commands.StartExecute(parking);
            Console.ReadKey();
             }
        
    }
}
